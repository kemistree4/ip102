""" Use pretrained IP102 Resnet50 model to classify images

>>> filename = 'compost-with-tiny-fly.jpg'
... from PIL import Image
... from torchvision import transforms
... input_image = Image.open(filename)
... preprocess = transforms.Compose([
...     transforms.Resize(256),
...     transforms.CenterCrop(224),
...     transforms.ToTensor(),
...     transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
... ])
... input_tensor = preprocess(input_image)
... input_batch = input_tensor.unsqueeze(0) # create a mini-batch as expected by the model
>>> model(input_batch)
tensor([[-0.8078,  0.5327, -0.5317,  0.8268, -1.2959,  0.5613, -0.3344,  1.0724,
          0.4316, -0.4786,  0.3345, -0.0615, -0.7352, -0.7594, -0.1741,  0.0154,
          ...
         -0.3935, -1.0071, -0.8707, -0.7857, -0.0451,  0.6112, -0.2618, -0.4088,
          0.2140,  0.0326,  0.4022,  0.4300,  0.5754,  2.9054]],
       grad_fn=<AddmmBackward>)
>>> output_tensor = _
>>> proba = torch.nn.functional.softmax(output_tensor[0], dim=0)
>>> len(proba)
102
>>> proba.argmax()
tensor(67, grad_fn=<NotImplemented>)
>>> class_names[proba.argmax()]
>>> ip102_class_names[proba.argmax()]
'Lycorma delicatula'
"""
import os
import pathlib
import urllib
import gzip
import re

from tqdm import tqdm

import torch
from torch import nn
from torchvision import models
from PIL import Image
from torchvision import transforms

DATA_DIR = pathlib.Path(__file__).parent
DATA_DIR = pathlib.Path(DATA_DIR).joinpath('pretrained_models')
os.makedirs(DATA_DIR, exist_ok=True)


class DownloadProgressBar(tqdm):
    """ Utility class that adds tqdm progress bar to urllib.request.urlretrieve

    >>>
    >>> with DownloadProgressBar(unit='B', unit_scale=True, miniters=1, desc=filename) as dpb:
    ...     urllib.request.urlretrieve(url, filename=dest_path, reporthook=dpb.update_to)
    None
    """

    def update_to(self, b=1, bsize=1, tsize=None):
        if tsize is not None:
            self.total = tsize
        self.update(b * bsize - self.n)


def download_if_necessary(
        url,
        dest_path=None,
        dest_dir=DATA_DIR):

    filename = pathlib.Path(url).name
    if not dest_path:
        dest_path = os.path.join(dest_dir, filename)
    if not pathlib.Path(dest_path).is_file():
        with DownloadProgressBar(unit='B', unit_scale=True, miniters=1, desc=filename) as dpb:
            urllib.request.urlretrieve(url, filename=dest_path, reporthook=dpb.update_to)
    return dest_path


def load_model(filename='resnet50_0.497.pkl.gz'):
    model_path = DATA_DIR.joinpath(filename)
    model_path = download_if_necessary(
        url='https://tan.sfo2.cdn.digitaloceanspaces.com/midata/public/models/cv/classifiers/resnet50_0.497.pkl.gz',
        dest_dir=DATA_DIR)

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    with gzip.open(model_path, 'rb') as model_file:
        state_dict = torch.load(model_file, map_location=device)

    model = models.resnet50()
    model.fc = nn.Linear(2048, 102)
    model.load_state_dict(state_dict)
    return model


def classnum_from_filepath(filepath):
    filepath = pathlib.Path(filepath)
    try:
        return int(re.match(r'^\d+', filepath.name).group())
    except (AttributeError, TypeError, ValueError):
        pass
    try:
        return int(re.match(r'^\d+', filepath.parent.name).group())
    except (AttributeError, TypeError, ValueError):
        pass
    return None


def load_class_names(filename='classes.txt'):
    ip102_classes = list(pathlib.Path(DATA_DIR).joinpath(filename).open())
    ip102_classes = [[x.strip() for x in s.split()] for s in ip102_classes]
    ip102_classes = sorted((int(x[0]), ' '.join(x[1:])) for x in ip102_classes)
    # first class is numbered "1" in classes.txt, but changes to 0 in this list
    return list(zip(*ip102_classes))[1]


def label_images(model, filepaths='*.jpg', class_names=None):
    # cifar10_labeled_images = torchvision.datasets.CIFAR10('cifar10', download=True)
    # cifar10_class_names = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
    class_names = class_names or load_class_names()
    if isinstance(filepaths, (str, pathlib.Path)):
        filepaths = list(pathlib.Path('.').rglob(filepaths))
    # labeled_images = [
    #     (PIL.Image.open(f.open('rb')).convert('RGB'), classnum_from_filepath(f) or 0)
    #     for f in filepaths
    # ]
    # testloader = torch.utils.data.DataLoader(labeled_images, batch_size=1, shuffle=False, num_workers=1)
    preprocess = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    ])
    labels = []
    for filename in filepaths:
        # filename = pathlib.Path('compost-with-tiny-fly.jpg')
        input_image = Image.open(filename)
        input_tensor = preprocess(input_image)
        input_batch = input_tensor.unsqueeze(0)  # create a mini-batch as expected by the model
        output_tensor = model(input_batch)
        proba = torch.nn.functional.softmax(output_tensor[0], dim=0)
        class_num = proba.argmax()
        class_name = class_names.get(proba.argmax(), 'unknown')
        labels.append((filename, class_num, class_name))
    return labels


def main():
    model = load_model()
    results = label_images(model)
    return model, results


if __name__ == '__main__':
    model, results = main()
